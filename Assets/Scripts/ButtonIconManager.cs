﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonIconManager : MonoBehaviour {
	public ButtonIcon[] ButtonIcons;

	public void UpdateIcons()
	{
		foreach (ButtonIcon icon in ButtonIcons)
		{
			icon.UpdateIcon ();
		}
	}
}
