﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Fungus;

public class ButtonIcon : MonoBehaviour {
	public Flowchart flowChart;

	public string AreaName;

	[Header("Textures and Colors")]
	public Sprite NoSprite;
	public Color NoColor;
	public Sprite YesSprite;
	public Color YesColor;
	public Sprite FriendsSprite;
	public Color FriendsColor;

	private Image _iconImage;

	// Use this for initialization
	void Awake () {
		_iconImage = GetComponent <Image> ();
		Debug.Log (_iconImage);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void UpdateIcon()
	{
		//here we read and update the various icons
		if (flowChart.GetBooleanVariable (AreaName+"V")==false)
		{
			_iconImage.color = Color.clear;
		}

		else if (flowChart.GetBooleanVariable (AreaName))
		{
			if (flowChart.GetIntegerVariable ("Score")<3)
			{
				_iconImage.sprite = YesSprite;
				_iconImage.color = YesColor;
			}
			else
			{
				_iconImage.sprite = FriendsSprite;
				_iconImage.color = FriendsColor;
			}
		}
		else
		{
			_iconImage.sprite = NoSprite;
			_iconImage.color = NoColor;
		}
	}

}
